package com.a_caring_reminder.app.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by darrankelinske on 10/20/15.
 */
public class SendSmsService extends JobService{

    private static final String LOG_TAG = SendSmsService.class.getSimpleName();

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i(LOG_TAG, "on start job: " + params.getJobId());

        Context context = getApplicationContext();
        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.i(LOG_TAG, "on stop job: " + params.getJobId());
        return true;
    }
}
