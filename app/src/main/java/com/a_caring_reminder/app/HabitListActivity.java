package com.a_caring_reminder.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.a_caring_reminder.app.data.AcrDB;
import com.a_caring_reminder.app.data.AcrQuery;
import com.a_caring_reminder.app.habits.HabitListAdapter;
import com.a_caring_reminder.app.models.Reminder;

import java.util.List;


/**
 * An activity representing a list of habits. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link HabitDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link HabitListFragment} and the item details
 * (if present) is a {@link HabitDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link HabitListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class HabitListActivity extends AppCompatActivity {


    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;


    //SQLite class
    com.a_caring_reminder.app.data.AcrDB AcrDB;

    //listItem type in models for listview
    private List<Reminder> ITEMS;
    private HabitListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();

                //Create Blank Detail Intent
                Intent detailIntent = new Intent(getApplicationContext(), HabitDetailActivity.class);
                //Start Detail Activity with Blank Intent
                startActivity(detailIntent);
            }
        });




        //Create the database here so that it gets the Activity Context.
        AcrDB = new AcrDB(getApplicationContext());
        AcrQuery query = new AcrQuery(AcrDB);
        try {
            if (ITEMS != null) {
                ITEMS.clear();
            }

            //if (!(ITEMS.get(0).getText().equals("No Items Yet")))
            // {

            ITEMS = query.remindersToList(AcrDB);
            adapter = new HabitListAdapter(
                    this,
                    ITEMS);




            //}


        } catch (Exception ex) {
            Log.d("Fill items", ex.getMessage());
        }

        ListView listView = (ListView) findViewById(R.id.item_list);
        listView.setAdapter(adapter);






    }

    /**
     * Callback method from {@link HabitListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */

    public void onItemSelected(String description) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(HabitDetailFragment.ARG_HABIT_ID, description);
            HabitDetailFragment fragment = new HabitDetailFragment();
            fragment.setArguments(arguments);
            getFragmentManager().beginTransaction()
                    .replace(R.id.habit_detail_container, fragment)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, HabitDetailActivity.class);
            detailIntent.putExtra(HabitDetailFragment.ARG_HABIT_ID, description);
            startActivity(detailIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        try{

            super.onCreateOptionsMenu(menu);

        }
        catch(Exception ex){
           //example
            Log.d("createMenuError", ex.getMessage());
        }

        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//
//            /*
//            case android.R.id.home:
//                // This ID represents the Home or Up button. In the case of this
//                // activity, the Up button is shown. Use NavUtils to allow users
//                // to navigate up one level in the application structure. For
//                // more details, see the Navigation pattern on Android Design:
//                //
//                // http://developer.android.com/design/patterns/navigation.html#up-vs-back
//                //
//                NavUtils.navigateUpTo(this, new Intent(this, TodaysActivity.class));
//                return true;
//                */
//
//            case R.id.messages:
//
//                Intent messageActivityIntent = new Intent(this, SupportMessagesActivity.class);
//                startActivity(messageActivityIntent);
//
//                return true;
//
//            case R.id.habits:
//                Intent habitListIntent = new Intent(this, HabitListActivity.class);
//                startActivity(habitListIntent);
//
//                return true;
//
//            case R.id.support:
//                Intent ListIntent = new Intent(this, SupportedListActivity.class);
//                startActivity(ListIntent);
//
//                return true;
//
//
//
//        }
//        return super.onOptionsItemSelected(item);
//    }



}
