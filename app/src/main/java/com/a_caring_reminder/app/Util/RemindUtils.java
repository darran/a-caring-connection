package com.a_caring_reminder.app.Util;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.telephony.SmsManager;
import android.util.Log;

import com.a_caring_reminder.app.HabitListActivity;
import com.a_caring_reminder.app.R;
import com.a_caring_reminder.app.data.AcrDB;
import com.a_caring_reminder.app.data.AcrQuery;
import com.a_caring_reminder.app.models.ScheduledAlarm;
import com.a_caring_reminder.app.service.SendSmsService;

/**
 * Created by daz on 3/19/15.
 */
public class RemindUtils {



        public static void dumpReminders (Context context, AcrDB acrDB)

        {

            String LOG_TAG = "Utils.dumpReminders";

            Log.i(LOG_TAG, "Attempting to dump Reminder table");


            Cursor db;
            try {
                db = acrDB.getReadableDatabase().rawQuery("Select * from Reminder", null);
                if (db.moveToFirst()) {
                    Log.i(LOG_TAG, "Dumping database cursor");
                    DatabaseUtils.dumpCursor(db);
                }

            }
            catch (Exception ex){
                Log.d(LOG_TAG, ex.getMessage());

            }

        }


    public static void sendSmsMessage(Context context, int mScheduledAlarmID) {

        String LOG_TAG = "Utils.sendSmSMessage";

        int NOTIFICATION_ID = 1;
        Uri soundURI = Uri
                .parse("android.resource://com.a_caring_reminder.app/"
                        + R.raw.alarm_rooster);
        long[] mVibratePattern = { 0, 200, 200, 300 };

        Intent resultIntent = new Intent(context, HabitListActivity.class);

        PendingIntent resultPendingIntent =
                TaskStackBuilder.create(context).addNextIntent(resultIntent).addParentStack(HabitListActivity.class).getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        AcrDB acrDB = new AcrDB(context);
        AcrQuery query = new AcrQuery(acrDB);
        Log.i("ACR", "Scheduled Alarm ID is "+ String.valueOf(mScheduledAlarmID));
        ScheduledAlarm mScheduledAlarm = query.getScheduledAlarm(String.valueOf(mScheduledAlarmID));
        Log.i("ACR", "The Habit ID for the alarm is  " + String.valueOf(mScheduledAlarm.getHabitID()));
        //String habitTitle = query.getHabitTime(String.valueOf(mScheduledAlarm.getHabitID())) + " "  + query.getHabitTitle(String.valueOf(mScheduledAlarm.getHabitID()));

        ScheduledAlarm scheduledAlarmObject = query.getScheduledAlarm(String.valueOf(mScheduledAlarmID));

        String habitNotificationTitle = "Sending message to " +query.getScheduledAlarmPhoneNumber(mScheduledAlarmID) ;

        Log.i("ACR", "Setting Habit Title as " + habitNotificationTitle);
        String messageText = query.getSupportMessageDetail(String.valueOf(query.getRandomSupportMessageID()));
        String habitNotificationText = "Message: " + mScheduledAlarm.getmMessage();
        Log.i("ACR", "Setting Message Text as " + messageText);




        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.acc_heart)
                .setContentTitle(habitNotificationTitle)
                .setContentText(habitNotificationText)
                .setVibrate(mVibratePattern);


        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, mBuilder.build());



        Log.i(LOG_TAG, "Sending SMS for reminder");

        SmsManager smsManager = SmsManager.getDefault();

        Log.i(LOG_TAG, "Sending SMS message to" + query.getScheduledAlarmPhoneNumber(mScheduledAlarmID));

        smsManager.sendTextMessage(query.getScheduledAlarmPhoneNumber(mScheduledAlarmID), null, mScheduledAlarm.getmMessage(), null, null);

    }

    public static void scheduleJob(Context context, int kJobId) {

        String LOG_TAG = "Utils.scheduleJob";

        Log.d(LOG_TAG, "Scheduling job: "+kJobId);

        ComponentName mServiceComponent;

        mServiceComponent = new ComponentName(context, SendSmsService.class);


        JobInfo.Builder builder = new JobInfo.Builder(kJobId++, mServiceComponent);
        builder.setMinimumLatency(Long.valueOf(60) * 1000);
        builder.setOverrideDeadline(Long.valueOf(1) * 1000);
        JobScheduler jobScheduler =
                (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);

        jobScheduler.schedule(builder.build());
    }

}
